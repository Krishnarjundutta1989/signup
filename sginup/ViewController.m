//
//  ViewController.m
//  sginup
//
//  Created by click labs 115 on 9/17/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    
    UIView *backimg1;
    UIView *backimg2;
    UIImageView *backimg;
    UIImageView *backimglogin;
    UIImageView *logologin;
    
    UILabel * lblfirstName;
    UILabel * lbllastName;
    UILabel * lblemailName;
    UILabel * password;
    UILabel * confirmPassword;
    UILabel * Phonenumber;
    UILabel *lblemailNamelogin;
    UILabel *Passwordlogin;
    
    UITextField *txtFirstName;
    UITextField *txtLastName;
    UITextField *txtEmail;
    UITextField *txtPhonenumber;
    UITextField *txtPassword;
    UITextField *txtConfirmPass;
    UITextField *txtEmailusersdetails;
    UITextField *txtPasswordusersdetails;
    
    
    int i;
    int count;
    
    // NSMutableArray *email;
    // NSMutableArray *pass;
    NSMutableArray *usersDetails;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    backimg1 = [UIView new];
    backimg1.frame = CGRectMake(1, 1, 373, 665);
    //backimg1.image = [UIImage imageNamed:@"Dust In Purple Light Artistic iPhone 6 Wallpaper.jpg"];
    [self.view addSubview:backimg1];
    
    
    backimg = [UIImageView new];
    backimg.frame = CGRectMake(1, 1, 373, 665);
    backimg.image = [UIImage imageNamed:@"Dust In Purple Light Artistic iPhone 6 Wallpaper.jpg"];
    
    [backimg1 addSubview:backimg];
    
    UIImageView *logo = [UIImageView new];
    logo.frame = CGRectMake(140, 40, 100, 100);
    logo.image = [UIImage imageNamed:@"Striped_apple_logo.png"];
    [backimg1 addSubview: logo];
    
    lblfirstName = [UILabel new];
    lblfirstName.frame = CGRectMake(20, 180, 155, 30);
    lblfirstName.text = @"First Name :";
    lblfirstName.textColor = [UIColor whiteColor];
    //lblfirstName.backgroundColor = [UIColor blackColor];
    [backimg1 addSubview:lblfirstName];
    
    lbllastName = [UILabel new];
    lbllastName.frame = CGRectMake(20, 220, 155, 30);
    lbllastName.text = @"Last Name :";
    lbllastName.textColor = [UIColor whiteColor];
    //lbllastName.backgroundColor = [UIColor blackColor];
    [backimg1 addSubview:lbllastName];
    
    lblemailName = [UILabel new];
    lblemailName.frame = CGRectMake(20, 260, 155, 30);
    lblemailName.text = @"Email :";
    lblemailName.textColor = [UIColor whiteColor];
    //lblemailName.backgroundColor = [UIColor blackColor];
    [backimg1 addSubview:lblemailName];
    
    Phonenumber = [UILabel new];
    Phonenumber.frame = CGRectMake(20, 300, 155, 30);
    Phonenumber.text = @"Phonenumber :";
    Phonenumber.textColor = [UIColor whiteColor];
    //Phonenumber.backgroundColor = [UIColor blackColor];
    [backimg1 addSubview:Phonenumber];
    
    password = [UILabel new];
    password.frame = CGRectMake(20, 340, 155, 30);
    password.text = @"Password :";
    password.textColor = [UIColor whiteColor];
    //password.backgroundColor = [UIColor blackColor];
    [backimg1 addSubview:password];
    
    
    confirmPassword = [UILabel new];
    confirmPassword.frame = CGRectMake(20, 380, 155, 30);
    confirmPassword.text = @"Confirm Password :";
    confirmPassword.textColor = [UIColor whiteColor];
    //confirmPassword.backgroundColor = [UIColor blackColor];
    [backimg1 addSubview:confirmPassword];
    
    
    
    
    
    
    
    
    txtFirstName = [UITextField new];
    txtFirstName.frame = CGRectMake(180, 180, 190, 30);
    txtFirstName.backgroundColor = [UIColor whiteColor];
    txtFirstName.placeholder = @"Enter First Name ";
    txtFirstName.textColor = [UIColor blackColor];
    [backimg1 addSubview:txtFirstName];
    
    txtLastName = [UITextField new];
    txtLastName.frame = CGRectMake(180, 220, 190, 30);
    txtLastName.backgroundColor = [UIColor whiteColor];
    txtLastName.placeholder = @"Enter Last Name ";
    txtLastName.textColor = [UIColor blackColor];
    [backimg1 addSubview:txtLastName];
    
    txtEmail = [UITextField new];
    txtEmail.frame = CGRectMake(180, 260, 190, 30);
    txtEmail.backgroundColor = [UIColor whiteColor];
    txtEmail.placeholder = @"Enter Email ";
    txtLastName.textColor = [UIColor blackColor];
    [backimg1 addSubview:txtEmail];
    
    txtPhonenumber = [UITextField new];
    txtPhonenumber.frame = CGRectMake(180, 300, 190, 30);
    txtPhonenumber.backgroundColor = [UIColor whiteColor];
    txtPhonenumber.placeholder = @"Enter PhoneNumber ";
    txtPhonenumber.textColor = [UIColor blackColor];
    txtPhonenumber.keyboardType = UIKeyboardTypePhonePad;
    [backimg1 addSubview:txtPhonenumber];
    
    
    txtPassword = [UITextField new];
    txtPassword.frame = CGRectMake(180, 340, 190, 30);
    txtPassword.backgroundColor = [UIColor whiteColor];
    txtPassword.placeholder = @"Enter Password ";
    txtPassword.secureTextEntry = YES;
    txtPassword.textColor = [UIColor blackColor];
    [backimg1 addSubview:txtPassword];
    
    
    txtConfirmPass = [UITextField new];
    txtConfirmPass.frame = CGRectMake(180, 380, 190, 30);
    txtConfirmPass.backgroundColor = [UIColor whiteColor];
    txtConfirmPass.placeholder = @"Enter ConfirmPassword ";
    txtConfirmPass.secureTextEntry = YES;
    txtConfirmPass.textColor = [UIColor blackColor];
    [backimg1 addSubview:txtConfirmPass];
    
    
    
    
    
    
    
    usersDetails = [NSMutableArray new];
    // pass = [NSMutableArray new];
    
    
    UIButton *btnsignup = [ UIButton new];
    btnsignup.frame = CGRectMake(150, 440, 80, 45);
    [btnsignup setImage:[UIImage imageNamed:@"singup.png"] forState:UIControlStateNormal];
    [btnsignup addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    [backimg1 addSubview:btnsignup];
    
    
    
}

- (void) pressed: (id) sender {
    
    
    
    if ([self checkSpecialCharecterForFirstName : txtFirstName.text  ] == YES ) {
        [self alertShowFirstNameAndSeconedName];
    }
    
    else if ([self checkSpecialCharecterForLastName : txtLastName.text  ] == YES){
        
        [self alertShowFirstNameAndSeconedName];
    }
    else if ([txtFirstName.text length] == 0){
        [self AllFieldMandatory];
        
    }
    else if ([txtLastName.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([txtEmail.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([txtPhonenumber.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([txtPassword.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([txtConfirmPass.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([self emailVAlidation : txtEmail.text ] == NO){
        [self emailAlert];
    }
    else if ([txtPassword.text length ] > 10 ) {
        [self PasswordMaxLength];
        
    }
    else if ([txtPassword.text length ] < 6){
        [self PasswordMinLength];
    }
    else if ([self phonenumbervalidation : txtPhonenumber.text] == YES){
        [self phoneNumberAlert];
    }
    else if ([self phoneNumberLength : txtPhonenumber.text] == YES ) {
        [self phoneNumberAlert];
    }
    else if ([self passwordValidation: txtConfirmPass.text] == YES){
        
        [self passwordMissMatchAlert];
    }/*
    else if ([self Passnumber : txtPassword.text ] == NO){
        [self passnumberMissingAlert];
    }*/

    else {
        
        
        [self signedinsuceesfully];
    }
}

-(BOOL) checkSpecialCharecterForFirstName : (NSString *) text{
    
    
    NSCharacterSet *set =  [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        NSLog(@"No Special Charecter");
        return NO;
    }
    else{
        NSLog(@"Has Special CHarecter");
        return YES;
    }
}


-(BOOL) checkSpecialCharecterForLastName : (NSString *) text{
    
    
    NSCharacterSet *set =  [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        NSLog(@"No Special Charecter");
        return NO;
    }
    else{
        NSLog(@"Has Special CHarecter");
        return YES;
    }
}


- (void)alertShowFirstNameAndSeconedName{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert"
                                                                   message:@"Only Alphabets Are Allowed :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)AllFieldMandatory{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert"
                                                                   message:@"All Field Are Mandatory :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (BOOL) emailVAlidation : (NSString *) sender {
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    //Valid email address
    
    if ([emailTest evaluateWithObject:txtEmail.text] == YES)
    {
        return YES;
    }
    else
    {
        NSLog(@"email not in proper format");
        return NO;
    }
}

- (void)emailAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In Email :"
                                                                   message:@"Please Enter Valid Email :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (BOOL)passwordValidation : (NSString *) sender {
    
    if ([txtPassword.text isEqualToString:txtConfirmPass.text]) {
        return NO;
    }
    else{
        return YES;
    }
}

- (void)PasswordMaxLength {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert In Password" message:@"Your Max Password Length At Least 10" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
    - (void)PasswordMinLength {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert In Password" message:@"Your Min Password Length At Least 6" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];

    
}
- (void)passwordMissMatchAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In ConfirmPassword :"
                                                                   message:@"PassWord Match Not Found :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}/*
    - (BOOL) Passnumber : (NSString *) sender {
        NSString *PassnumbersRegEx =      @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}";

        NSPredicate *PassnumbersTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PassnumbersRegEx];
        //Valid email address
        
        if ([PassnumbersTest evaluateWithObject:txtPassword.text] == YES)
        {
            return YES;
        }
        else
        {
            NSLog(@"Passnumber not in proper format");
            return NO;
        }
    }

- (void)passnumberMissingAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In Password :"
                                                                   message:@"PassWord Must containt one At Least NUmber :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
*/



- (BOOL)phoneNumberLength : (NSString *) sender {
    
    if ([txtPhonenumber.text length ] == 10) {
        return NO;
    }
    else{
        NSLog(@"Enter Valid Phn Number");
        return YES;
    }
    
    
}


-(BOOL) phonenumbervalidation : (NSString *) text{
    
    
    NSCharacterSet *set =  [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        NSLog(@"No Special Charecter");
        return NO;
    }
    else{
        NSLog(@"Has Special CHarecter");
        return YES;
    }
}

- (void)phoneNumberAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In Email :"
                                                                   message:@"Please Enter Valid 10 Digits PhoneNumber :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)signedinsuceesfully{
   
    [self loginsuessfully];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Sucessfull :"
                                                                   message:@"You SignedUp Sucessfully :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)loginsuessfully {
    [ usersDetails addObject:txtEmail.text]; //0
    [ usersDetails addObject:txtPassword.text]; //1
    
    
    
    backimg2 = [UIView new];
    backimg2.frame = CGRectMake(1, 1, 373, 665);
    //backimg2.image = [UIImage imageNamed:@"Dust In Purple Light Artistic iPhone 6 Wallpaper.jpg"];
    backimg2.backgroundColor = [UIColor redColor];
    [self.view addSubview:backimg2];
    
    backimglogin = [UIImageView new];
    backimglogin.frame = CGRectMake(1, 1, 373, 665);
    backimglogin.image = [UIImage imageNamed:@"Dust In Purple Light Artistic iPhone 6 Wallpaper.jpg"];
    [backimg2 addSubview:backimglogin];

    
    logologin = [UIImageView new];
    logologin.frame = CGRectMake(140, 40, 100, 100);
    logologin.image = [UIImage imageNamed:@"Striped_apple_logo.png"];
    [backimg2 addSubview: logologin];
    
    lblemailNamelogin = [UILabel new];
    lblemailNamelogin.frame = CGRectMake(20, 260, 155, 30);
    lblemailNamelogin.text = @"Email :";
    lblemailNamelogin.textColor = [UIColor whiteColor];
    //lblemailName.backgroundColor = [UIColor blackColor];
    [backimg2 addSubview:lblemailNamelogin];
    
    Passwordlogin = [UILabel new];
    Passwordlogin.frame = CGRectMake(20, 300, 155, 30);
    Passwordlogin.text = @"Password :";
    Passwordlogin.textColor = [UIColor whiteColor];
    //Phonenumber.backgroundColor = [UIColor blackColor];
    [backimg2 addSubview:Passwordlogin];


    
    txtEmailusersdetails = [UITextField new];
    txtEmailusersdetails.frame = CGRectMake(180, 260, 190, 30);
    txtEmailusersdetails.backgroundColor = [UIColor whiteColor];
    txtEmailusersdetails.placeholder = @"Enter Email ";
    txtLastName.textColor = [UIColor blackColor];
    [backimg2 addSubview:txtEmailusersdetails];
    
    txtPasswordusersdetails = [UITextField new];
    txtPasswordusersdetails.frame = CGRectMake(180, 300, 190, 30);
    txtPasswordusersdetails.backgroundColor = [UIColor whiteColor];
    txtPasswordusersdetails.placeholder = @"Enter Password ";
    txtPasswordusersdetails.secureTextEntry = YES;
    txtPasswordusersdetails.textColor = [UIColor blackColor];
    [backimg2 addSubview:txtPasswordusersdetails];
    
    UIButton *btnsignin = [ UIButton new];
    btnsignin.frame = CGRectMake(150, 440, 80, 45);
    [btnsignin setImage:[UIImage imageNamed:@"login.png"] forState:UIControlStateNormal];
    [btnsignin addTarget:self action:@selector(signedin:) forControlEvents:UIControlEventTouchUpInside];
    [backimg2 addSubview:btnsignin];
    

}


- (void)signedin: (id) sender {
    
    if (([txtPasswordusersdetails.text isEqualToString:[usersDetails objectAtIndex:1]])&&([txtEmailusersdetails.text isEqualToString:[usersDetails objectAtIndex:0]]))
    {
        [ self signinSucessfull];
    }
    else
    {
        [self signinAlert];
    }
}

- (void)signinAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In signin :"
                                                                   message:@"Please Enter Valid Email And Password :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)signinSucessfull{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Sucessfully signin :"
                                                                   message:@"You SignedIn Sucessfully :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}






// Do any additional setup after loading the view, typically from a nib.


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
